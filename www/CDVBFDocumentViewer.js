var exec = require('cordova/exec');

var CDVBFDocumentViewer = {
  open: function(options, onSuccess, onError){
    options = options || {};
    exec(onSuccess, onError, "CDVBFDocumentViewer", "open",[options]);
  },
  close:function(onSuccess,onError){
    exec(onSuccess, onError, "CDVBFDocumentViewer", "close",[{}])
  }
}

module.exports = CDVBFDocumentViewer;