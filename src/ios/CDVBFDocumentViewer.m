

#import <Cordova/CDV.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface CDVBFDocumentViewer : CDVPlugin <UIDocumentInteractionControllerDelegate> {
    Boolean run;
}

@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController;
@property (nonatomic, strong) NSMutableArray *documentURLs;
@property (nonatomic, copy) NSString* callbackId;

- (void)open:(CDVInvokedUrlCommand*)command;
- (void)close:(CDVInvokedUrlCommand*)command;
@end

@implementation CDVBFDocumentViewer

- (void)setupDocumentControllerWithURL:(NSURL *)url andTitle:(NSString *)title
{
    if (self.docInteractionController == nil) {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.name = title;
        self.docInteractionController.delegate = self;
    } else {
        self.docInteractionController.name = title;
        self.docInteractionController.URL = url;
    }
}

- (void)documentInteractionControllerDidEndPreview:(UIDocumentInteractionController *)controller
{
    NSLog(@"User has dismissed preview");
    
    if (self.callbackId != nil) {
        NSString * cbid = [self.callbackId copy];
        self.callbackId = nil;
        [self sendPluginResultStringWithId:@"closed" :cbid];        
    }
}

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL
                                               usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {

    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;

    return interactionController;
}

- (UIViewController *) documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *) controller {
    //NSLog(@"documentInteractionControllerViewControllerForPreview");
    run = false;
    return self.viewController;
}

- (void)close:(CDVInvokedUrlCommand*)command{
  //NSLog(@"close");
  if(self.docInteractionController != nil){
    [self.docInteractionController dismissPreviewAnimated:YES];
    self.docInteractionController = nil;
  }
  
  if (self.callbackId != nil) {
    NSString * cbid = [self.callbackId copy];
    self.callbackId = nil;
    [self sendPluginResultStringWithId:@"cancelled" :cbid];
  }
}

- (void)open:(CDVInvokedUrlCommand*)command
{
    //self.docInteractionController = nil;
  self.callbackId = command.callbackId;

    if (run == false) {
        run = true;
        
        NSDictionary* options = [command.arguments objectAtIndex:0];
        NSString* url = options[@"url"];
        NSString* title = options[@"filename"];
        NSString* timeout = options[@"timeout"];

        if (url != nil && [url length] > 0) {
            [self.commandDelegate runInBackground:^{
                self.documentURLs = [NSMutableArray array];

                NSURL *URL = [self localFileURL:url withFileName:title withTimeout:timeout];

                if (URL) {

                  
                    [self.documentURLs addObject:URL];
                    [self setupDocumentControllerWithURL:URL andTitle:title];
                    double delayInSeconds = 0.1;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                      [self.docInteractionController presentPreviewAnimated:YES];
                      
                      [self sendPluginResultStringWithId:@"loaded" :self.callbackId];
                    
                    });
                } else {
                  run = false;
                  [self sendPluginResultStringWithId:@"error" :self.callbackId];
                }
            }];
        } else {
            run = false;
            [self sendPluginResultStringWithId:@"error" :self.callbackId];
        }
    } else {
        run = false;
        [self sendPluginResultStringWithId:@"error" :self.callbackId];
    }
}

- (NSURL *)localFileURL:(NSString *)docUrl withFileName:(NSString *)filename withTimeout:(NSString *)timeout
{
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    NSURL *fileURL = [NSURL URLWithString:docUrl];
    if ([fileURL isFileReferenceURL]) {
        return fileURL;
    }
    
    //NSData *data = [NSData dataWithContentsOfURL:fileURL];
    double requestTimeout = [timeout doubleValue];
    NSURLResponse *urlResponse;
    NSError *error;
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:fileURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:requestTimeout];
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&urlResponse error:&error];
    if( data ) {
        fileURL = [tmpDirURL URLByAppendingPathComponent:filename];
        [[NSFileManager defaultManager] createFileAtPath:[fileURL path] contents:data attributes:nil];
        return fileURL;
    } else {
        return nil;
    }
}

-(void)sendPluginResultString: (NSString *)result :(CDVInvokedUrlCommand *)command{
  CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"event":result}];
  [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
  [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)sendPluginResultStringWithId: (NSString *)result :(NSString *)commandId{
  CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"event":result}];
  [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
  [self.commandDelegate sendPluginResult:pluginResult callbackId:commandId];
}



@end
